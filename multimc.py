import os
import shutil
import PySimpleGUI as sg
import lib
import tempfile
import re

def export_multimc(pack_src, dest_dir, name, pack_toml, vers):
    mmc_export = f"{pack_src}/mmc_export"
    if os.path.isdir(mmc_export):
        shutil.rmtree(mmc_export, ignore_errors=True)
    os.mkdir(mmc_export)
    mmc_export_mc = f"{mmc_export}/.minecraft"
    os.mkdir(mmc_export_mc)
    overrides_dir = f"{pack_src}/overrides"
    defaults_dir = f"{pack_src}/defaults"
    shutil.copytree(f"{overrides_dir}/config", f"{mmc_export_mc}/config")
    shutil.copytree(f"{overrides_dir}/mods", f"{mmc_export_mc}/mods")
    shutil.copy(f"{overrides_dir}/servers.dat", mmc_export_mc)
    shutil.copy(f"{overrides_dir}/{name}.png", mmc_export)
    shutil.copy(f"{overrides_dir}/mmc-pack.json", mmc_export)
    shutil.copy(f"{defaults_dir}/options.txt", mmc_export_mc)
    shutil.copy(f"{defaults_dir}/instance.cfg", mmc_export)
    lib.apply_version_updates(pack_toml, vers, mmc_export_mc)
    shutil.make_archive(dest_dir + "/" + name, "zip", mmc_export)

def update_multimc(pack_src, instance_dir, name, pack_toml, vers):
    mc_dir = f"{instance_dir}/.minecraft"
    overrides_dir = f"{pack_src}/overrides"
    shutil.rmtree(f"{mc_dir}/config")
    shutil.rmtree(f"{mc_dir}/mods")
    shutil.copytree(f"{overrides_dir}/config", f"{mc_dir}/config")
    shutil.copytree(f"{overrides_dir}/mods", f"{mc_dir}/mods")
    shutil.copy(f"{overrides_dir}/servers.dat", f"{mc_dir}/")
    shutil.copy(f"{overrides_dir}/mmc-pack.json", f"{instance_dir}/")

    icon_dir = f"{instance_dir}/../../icons"
    shutil.copy(f"{overrides_dir}/{name}.png", f"{icon_dir}/")
    instance_cfg = f"{instance_dir}/instance.cfg"
    with open(instance_cfg, "r") as sources:
        lines = sources.readlines()
    with open(instance_cfg, "w") as sources:
        for line in lines:
            sources.write(re.sub("^iconKey=.*$", f"iconKey={name}", line))
    lib.apply_version_updates(pack_toml, vers, instance_dir + "/.minecraft/")

if __name__ == "__main__":
    layout = [ [sg.Text("Modpack URL"), sg.InputText(key="-url-", enable_events=True), sg.Button("Download", key="-Download-")],
    [sg.Text("Modpack: ", key="-modpackname-")],
    [sg.Text("Instance Directory"), sg.InputText(key='-instancedir-'), sg.FolderBrowse(target="-instancedir-"), sg.Button("Update Instance", key="-updateinstance-")],
    [sg.Text("Destination MultiMC ZIP directory"), sg.InputText(key='-dest_zip_dir-'), sg.FolderBrowse(target="-dest_zip_dir-"), sg.Button("Export ZIP", key="-export_zip-")] ]

    window = sg.Window("Overcomplicated Modpack Updater", layout)

    td = ""
    pack_toml, vers, pack_src  = (None,None,None)
    try:
        while True:
            event, values = window.read()
            if event == sg.WIN_CLOSED:
                break
            elif event == "-Download-":
                print("Downloading modpack..")
                if td != "":
                    shutil.rmtree(td, ignore_errors=True)
                
                td = tempfile.mkdtemp()
                pack_toml, vers, pack_src  = (None,None,None)
                window["-modpackname-"].update(f"Modpack:")
                try:
                    pack_toml, vers, pack_src = lib.extract_modpack(td, lib.download_modpack(td, values["-url-"]))
                    sg.popup(f"Successfully downloaded modpack!")
                    window["-Download-"].update(disabled=True)
                    modpackname = pack_toml.get("name", "(no name)")
                    window["-modpackname-"].update(f"Modpack: {modpackname} {vers}")
                except Exception as e:
                    sg.popup(f"Error downloading modpack: {e}")
            elif event == "-url-":
                window["-Download-"].update(disabled=False)
            elif event == "-updateinstance-":
                if not pack_src:
                    sg.popup(f"Download and extract a modpack first!")
                    continue
                modpackname = pack_toml.get("name", "untitled_modpack")
                instance_dir = values["-instancedir-"]
                try:
                    update_multimc(pack_src, instance_dir, modpackname, pack_toml, vers)
                    sg.popup(f"Successfully updated MultIMC instance at {instance_dir}!")
                except Exception as e:
                    sg.popup(f"Error updating instance: {e}")
                    
            elif event == "-export_zip-":
                if not pack_src:
                    sg.popup(f"Download and extract a modpack first!")
                    continue
                modpackname = pack_toml.get("name", "untitled_modpack")
                dest_zip = values["-dest_zip_dir-"] + f"/{modpackname}.zip"
                
                try:
                    export_multimc(pack_src, values["-dest_zip_dir-"], modpackname, pack_toml, vers)
                    sg.popup(f"Successfully exported MultIMC instance to {dest_zip}!")
                except Exception as e:
                    sg.popup(f"Error exporting modpack to zip: {e}")                    

        window.close()
    finally:
        if td != "":
            shutil.rmtree(td, ignore_errors=True)