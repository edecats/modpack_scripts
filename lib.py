#!/usr/bin/python

import argparse
import sys
import zipfile
import os
import requests
import tempfile
import toml
import re

def download_modpack(td, pack_url):
    r = requests.get(pack_url, allow_redirects=True)
    with open(f"{td}/pack.zip", 'wb') as f:
        f.write(r.content)
    return f"{td}/pack.zip"

def extract_modpack(td, pack_path):
    zp = pack_path
    with zipfile.ZipFile(zp) as z:
        vers = z.comment.decode()
        vers = vers[0:7] if vers else "DEV"
        # parent directory in archive
        pfx = z.infolist()[0].filename
        pack_toml_present = False
        for file in z.namelist():
            if file == f"{pfx}pack.toml":
                pack_toml_present = True
                z.extract(file,td)
            elif file.startswith(f"{pfx}overrides/") or file.startswith(f"{pfx}defaults/"):
                z.extract(file,td)
        if not pack_toml_present:
            raise RuntimeError("Malformatted modpack archive?")
    
    pack_toml = toml.load(f"{td}/{pfx}/pack.toml")
    return (pack_toml, vers, f"{td}/{pfx}")

def apply_version_updates(pack_toml, vers, instance_dir):
    for version_patch_file in pack_toml.get("version_patch_files", []):
        vpfp = f"{instance_dir}/{version_patch_file['path']}"
        search = version_patch_file["search"]
        replace = version_patch_file["replace"]
        try:
            updated = False
            with open(vpfp, "r") as sources:
                lines = sources.readlines()
            with open(vpfp, "w") as sources:
                for line in lines:
                    repl_line = re.sub(search, replace.format(vers), line)
                    if repl_line != line:
                        updated = True
                    sources.write(repl_line)
            if not updated:
                print(f"Found {vpfp}, but did not update it.")
        except FileNotFoundError:
            print(f"Skipping {vpfp} as it could not be found")
            pass




